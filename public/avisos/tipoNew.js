var datosFormulario; //lo que se va a enviar
var nombre;
var descripcion;
$(function(){

	$("input:submit").click(function() {
		datosFormulario= $('#formNew');
		nombre = document.getElementById('nombre').value;
		descripcion = document.getElementById('descripcion').value;
		if(descripcion== "" || nombre == ""){
			mostrarAviso(2);
			return false;
		}
		obtenerMensaje();
		return false;
	});

});
function mostrarAviso(error){
	switch(error) {
	    case 1:
	        $("#aviso").html("<div class='alert alert-danger alert-dismissable'><button type='button' class='close'"
		 	+"data-dismiss='alert' aria-hidden='true'>&times;</button>El nombre ya esta registrado!.</div>");
	        break;
	    case 2:
	        $("#aviso").html("<div class='alert alert-danger alert-dismissable'><button type='button' class='close'"
		 	+"data-dismiss='alert' aria-hidden='true'>&times;</button>Ambos campos son necesarios!</div>");
	        break;	
	}
}
function obtenerMensaje() {
    $.ajax({
        url: '/tipos/new',
        type: 'POST',
        data: datosFormulario.serialize(),
        success : function(data) {
        	var arreglo=Object.values(data);
        	if(arreglo[1]==3) window.location.replace("/tipos");
            mostrarAviso(arreglo[1]);
        }
    });
}