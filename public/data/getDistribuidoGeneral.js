var formularioDistribuido;
// funcion que agrega las nuevas filas a la tabla
function agregarFilas(productos) {
    console.log('general')
    for (var i = 0; productos[i]; i++) {
        var producto = productos[i],
            nombre = producto.nombreProducto,
            codigo = producto.codigo,
            cantidadDistribuido = producto.cantidadDistribuido;

        getFilas(nombre, codigo, cantidadDistribuido);
    }
    $('#dataTables-example').DataTable().draw();
}

function getFilas(nombre, codigo, cantidadDistribuido) {
    var table = $('#dataTables-example').DataTable();
    var string1, string2, string3;
    // llenar el string :(
    string1 = '<td>' + nombre + '</td>';
    string2 = '<td>' + codigo + '</td>';
    string3 = '<td>' + cantidadDistribuido + '</td>';

    table.row.add([
        string2,
        string1,
        string3
    ]);
}

// elimina todas las filas de la tabla, menos la principal
function eliminaFilas() {
    //$("#tbodyid").empty();    
    $('#dataTables-example').DataTable().clear().draw();
};

// obtencion de los datos para el top ten
function obtenerDistribuido() {
    $.ajax({
        url: '/distribuido',
        type: 'POST',
        data: formularioDistribuido.serialize(),
        success: function (data) {
            // Distribuido
            agregarFilas(data);
        }
    });
}

// obtencion de los datos para el top ten
function reiniciarDistribuido() {
    $.ajax({
        url: '/distribuido',
        type: 'POST',
        data: formularioDistribuido.serialize(),
        success: function (data) {
            // Almacen
            eliminaFilas();
            agregarFilas(data);
        }
    });
}

// funcion principal
$(function () {
    // obtengo el formulario del almacen
    formularioDistribuido = $('#formdistribuido');
    obtenerDistribuido();

    // fechas para el top ten
    $("select[name=plaza]").change(function () {
        reiniciarDistribuido();
    });
    // select de sucursal
    $("select[name=categoria]").change(function () {
        reiniciarDistribuido();
    });
});