var formularioDistribucion;
// funcion que agrega las nuevas filas a la tabla
function agregarFilas(productos){
    for(var i=0 ; productos[i] ; i++){
        var producto = productos[i],
            nombre = producto.nombreProducto,
            codigo = producto.codigo,
            cantidadDistribucion = producto.cantidadDistribucion;

        getFilas(nombre, codigo, cantidadDistribucion);
    }
    $('#dataTables-example').DataTable().draw();
}

function getFilas(nombre, codigo, cantidadDistribucion){
    var table = $('#dataTables-example').DataTable();
    var string1,string2,string3;
    // llenar el string :(
    string1 = '<td>' + nombre + '</td>';
    string2 = '<td>' + codigo + '</td>';
    string3 = '<td>' + cantidadDistribucion + '</td>';

    table.row.add([
        string2,
        string1,
        string3
    ]);
}

// elimina todas las filas de la tabla, menos la principal
function eliminaFilas(){
    //$("#tbodyid").empty();    
    $('#dataTables-example').DataTable().clear().draw();
};

// obtencion de los datos para el top ten
function obtenerDistribucion() {
    $.ajax({
        url: '/distribucion',
        type: 'POST',
        data: formularioDistribucion.serialize(),
        success : function(data) {
            // Distribucion
            agregarFilas(data);
        }
    });
}

// obtencion de los datos para el top ten
function reiniciarDistribucion() {
    $.ajax({
        url: '/distribucion',
        type: 'POST',
        data: formularioDistribucion.serialize(),
        success : function(data) {
            // Almacen
            eliminaFilas();
            agregarFilas(data);
        }
    });
}

// funcion principal
$(function(){ 
    // obtengo el formulario del almacen
    formularioDistribucion = $('#formdistribucion');
    obtenerDistribucion();

    // fechas para el top ten
    $("select[name=plaza]").change(function(){
        reiniciarDistribucion();
    });
        // select de sucursal
    $("select[name=categoria]").change(function(){
        reiniciarDistribucion();
    });
});