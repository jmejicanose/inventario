/**
 * Created by Jorge Mejicanos
 */
module.exports = function(req,res,next){
    if(req.session.user){
        next()
    }else{
        res.redirect("/login")
    }
}