/**
 * Created by Jorge Mejicanos
 */
'use strict'

const express = require("express"),
    ProductoController = require('../controllers/producto'),
    producto = express.Router()
// inventario/products
producto
    .route('/')
    .get(  ProductoController.productsGet)
    .post( ProductoController.productsPost)
// inventario/products/new
producto
    .route("/new")
    // Metodo GET
    .get( ProductoController.productsNewGet )
    // Metodo POST
    .post( ProductoController.productsNewPost )
// inventario/products/:idProducto
producto
    .route("/:id")
    // Metodo GET
    .get( ProductoController.productsIdProductoGet )
    // Metodo PUT
    .put( ProductoController.productsIdProductoPut )
    // Metodo DELETE
    .delete( ProductoController.productsIdProductoDelete )

module.exports = producto