/**
 * Created by Jorge Mejicanos
 */
'use strict'

const express = require("express"),
    HistorialController = require('../controllers/historial'),
    historial = express.Router()

// inventario/historial/movimientos
historial
    .route('/movimientos')
    .get( HistorialController.historialMovimientosGet )
    .post( HistorialController.historialMovimientosPost )
// inventario/historial/bajas
historial
    .route('/bajas')
    .get( HistorialController.historialBajasGet )
    .post( HistorialController.historialBajasPost )
// inventario/historial/general
// Metodo GET
historial.get("/general", HistorialController.historialGeneralGet )
// inventario/historial/sucursal
// Metodo GET
historial.get("/sucursal", HistorialController.historialSucursalGet )
// inventario/historial/sucursaltop
// Metodo POST
historial.post("/sucursaltop", HistorialController.historialSucursalTopPost )
// inventario/historial/sucursalbasicos
// Metodo POST
historial.post("/sucursalbas", HistorialController.historialSucursalBasicosPost )
// inventario/historial/generaltop
// Metodo POST
historial.post("/generaltop", HistorialController.historialGeneralTopPost )
// inventario/historial/generalbas
// Metodo POST
historial.post("/generalbas", HistorialController.historialGeneralBasicosPost )

module.exports = historial;