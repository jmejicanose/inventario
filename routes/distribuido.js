/**
 * Created by Jorge Mejicanos
 */
'use strict'

const express = require("express"),
    DistribuidoController = require('../controllers/distribuido'),
    distribuido = express.Router()

// inventario/distribuido
distribuido.get("/", DistribuidoController.distribuidoGet)
// inventario/categoria/categoria/get
distribuido.post('/', DistribuidoController.distribuidoPost)
// inventario/distribuido/:id
distribuido.put("/:id", DistribuidoController.distribuidoIdDistribuidoPut)

module.exports = distribuido