/**
 * Created by Jorge Mejicanos
 */
'use strict'

const express = require('express'),
    TipoController = require('../controllers/tipo'),
    tipo = express.Router()

tipo
    .get('/', TipoController.tiposGet )

tipo
    .route('/new')
    .get( TipoController.tiposNewGet )
    .post( TipoController.tiposNewPost )

tipo
    .route('/:id')
    .get( TipoController.tiposIdTipoGet )
    .put( TipoController.tiposIdTipoPut )
    .delete( TipoController.tiposIdTipoDelete )
    
module.exports = tipo
