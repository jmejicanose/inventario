/**
 * Created By Jorge Mejicanos
 */
'use strict'

const express = require("express"),
    AlmacenController = require('../controllers/almacen'),
    almacen = express.Router()

// inventario/almacen
almacen
    .route("/")
    .get(  AlmacenController.almacenGet )
    .post( AlmacenController.almacenPost )
// inventario/almacen/:id/add
almacen.put('/:id/add' , AlmacenController.almacenIdAlmacenAddPut )
// inventario/almacen/:idAlmacen/sub
almacen.put('/:id/sub' , AlmacenController.almacenIdAlmacenSubPut )
 
module.exports = almacen