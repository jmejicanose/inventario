/**
 * Created by Jorge Mejicanos
 */
'use strict'

const SucursalModel = require('../models/sucursal'),
    ProductModel = require('../models/producto'),
    AlmacenModel = require('../models/almacen'),
    TipoModel = require('../models/tipo'),
    Utilidad = require('../ayuda/utilidad')

function sucursalesGet(req, res) {
    // busco las sucursales
    SucursalModel.getSucursales((error, sucursales) => {
        (error) ? ( // si hubo error
            Utilidad.printError(res, { msg: `Error al obtener las sucursales: ${error}`, tipo: 0 })
        ) : ( // si no hubo error
                res.render('./sucursales/manager', { sucursales, usuario: req.session.user })
            )
    })
}

function sucursalesNewGet(req, res) {
    TipoModel.getNamesOfTipos( (error, tipos) => { // si no hubo error
        (error) ? (
            Utilidad.printError(res, { msg: `Error al obtener los tipos: ${error}`, tipo: 0})
        ) : (
            res.render("./sucursales/new",{ usuario: req.session.user, tipos })
        )
    })
}

function sucursalesNewPost(req, res) {
    let nombreTipo = req.body.tipo
    TipoModel.getIdTipoByNombre(nombreTipo, (error, idTipo) => {
        if (error) {
            Utilidad.printError(res, { msg: `Error al buscar el id del tipo: ${error}`, tipo: 0 })
            return
        }
        // creo la nueva sucursal
        let nuevaSucursal = {
            plaza: req.body.plaza,
            ciudad: req.body.ciudad,
            idTipo
        }
        // guardo la nueva sucursal en la base de datos
        SucursalModel.createSucursal(nuevaSucursal, error => {
            if (error) { // si hubo error
                Utilidad.printError(res, { msg: `Error al guardar la nueva sucursal, plaza repetida: ${error}`, tipo: 1 })
            } else { // si no hubo error
                //res.redirect('/sucursales')
                res.json({ msg: "", tipo: 3 })
                // genero los almacenes para la sucursal
                // con los productos existentes
                // busco el id de la sucursal que se acaba de crear
                SucursalModel.getIdSucursalByPlaza(nuevaSucursal.plaza, (error, id) => {
                    (error) ? (
                        Utilidad.printError(res, { msg: `Error al buscar el id de la sucursal: ${error}`, tipo: 0 })
                    ) : (
                            generarAlmacenes(req, res, id)
                        )
                })
            }
        })
    })
}

function sucursalesIdSucursalGet(req, res) {
    // declaro variables necesarias
    let id = req.params.id,
        usuario = req.session.user
    TipoModel.getNamesOfTipos((error, tipos) => { // si no hubo error
        if (error) {
            Utilidad.printError(res, { msg: `Error al obtener los tipos: ${error}`, tipo: 0 })
            return
        }
        // busco la sucursal a editar
        SucursalModel.getSucursalById(id, (error, sucursalUpdate) => {
            (error) ? ( // si hubo error
                Utilidad.printError(res, { msg: `Error al obtener la sucursal: ${error}`, tipo: 0 })
            ) : ( // si no hubo error
                    (comprobarSucursal(sucursalUpdate)) ? (
                        res.render('./sucursales/update', { usuario, tipos, sucursalUpdate })
                    ) : (
                            res.redirect('/sucursales')
                        )

                )
        })
    })
}

function sucursalesIdSucursalPut(req, res) {
    let nombreTipo = req.body.tipo;
    // obtengo la sucursal con los cambios realizados
    TipoModel.getIdTipoByNombre(nombreTipo, (error, idTipo) => {
        if (error) {
            Utilidad.printError(res, { msg: `Error al buscar el id del tipo: ${error}`, tipo: 0 })
            return
        }
        let sucursalUpdate = {
            id: req.params.id,
            plaza: req.body.plaza,
            ciudad: req.body.ciudad,
            idTipo
        }
        // actualizo la sucursal en la base de datos
        SucursalModel.updateSucursal(sucursalUpdate, error => {
            (error) ? ( // si hubo error
                Utilidad.printError(res, { msg: `Error al actualizar sucursal, plaza repetida: ${error}`, tipo: 1 })
            ) : ( // si no hubo error
                    //res.redirect('/sucursales')
                    res.json({ msg: "", tipo: 3 })
                )
        })
    })
}
// pendiente, no se como eliminar por cascada, aun
function sucursalesIdSucursalDelete(req, res) {
    let id = req.params.id
    // elimina la sucursal, y realiza eliminacion por cascada
    SucursalModel.deleteSucursal(id, error => {
        if (error) Utilidad.printError(res, { msg: `Error al eliminar la sucursal: ${error}`, tipo: 0 })
        res.redirect('/sucursales')
    })
}

function generarAlmacenes(req, res, id) {
    // busco todos los productos registrados
    ProductModel.getAllIdProducto((error, productos) => {
        if (error) {
            Utilidad.printError(res, { msg: `Error al obtener los ids de los productos: ${error}`, tipo: 0 })
        } else {
            let values = []
            for (let i = 0; productos[i]; i++) {
                values.push([id, productos[i].id])
            }

            AlmacenModel.createAlmacenes(values, (error) => {
                if (error) Utilidad.printError(res, { msg: `Error al crear el almacen: ${error}`, tipo: 1 })
            })
        }
    })
}

function comprobarSucursal(sucursalUpdate) {
    try {
        return sucursalUpdate.id != null
    } catch (error) {
        return false
    }
}

module.exports = {
    sucursalesGet,
    sucursalesNewGet,
    sucursalesNewPost,
    sucursalesIdSucursalGet,
    sucursalesIdSucursalPut,
    sucursalesIdSucursalDelete
}