/**
 * Created by Jorge Mejicanos
 */
module.exports = {
    PORT : 3000,
    PUBLIC : '/public',
    VIEWS: './views'
}