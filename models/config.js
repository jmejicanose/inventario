/**
 * Created by Jorge Mejicanos
 */
module.exports = {
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: '',
    database: 'inventario'
}
