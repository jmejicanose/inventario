/**
 * Created by Jorge Mejicanos
 */
'use strict'

const mysql = require('mysql'),
    //dbOptions = require('./config'),
    //myConnection = mysql.createConnection(dbOptions)
    myConnection = mysql.createConnection({
        host     : 'localhost',
        database : 'inventario',
        user     : 'root',
        password : 'root',
    })

myConnection.connect( err => {
    return (err) ? console.log('Error al conectarse a mysql: '+err.stack) : console.log('Coneccion establecida con mysql')
})

module.exports = myConnection