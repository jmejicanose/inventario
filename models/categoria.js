/**
 * Created by Jorge Mejicanos
 */
'use strict'

const CategoryModel = require('./coneccion')

function getCategoryById(id, next) {
    CategoryModel
        .query(`SELECT * 
                FROM categorias 
                WHERE id = ?`, id, (error, resultado, fields) => {

                next(error, resultado[0])
            })
}

function getIdCategoryByName(name, next) {
    CategoryModel
        .query(`SELECT c.id 
                FROM categorias c 
                WHERE c.nombre = ?`, name, (error, resultado, fields) => {

                (typeof resultado[0] === 'undefined') ? next(error, null) : next(error, resultado[0].id)
            })
}

function getCategories(next) {
    CategoryModel
        .query(`SELECT * 
                FROM categorias` , (error, resultado, fields) => {

                next(error, resultado)
            })
}

function getNamesOfCategories(next) {
    CategoryModel
        .query(`SELECT c.nombre 
                FROM categorias c` , (error, resultado, fields) => {

                next(error, resultado)
            })
}

function createCategory(category, next) {
    CategoryModel
        .query(`INSERT INTO categorias 
                SET ?`, category, (error, resultado, fields) => {

                next(error)
            })
}

function updateCategory(category, next) {
    CategoryModel
        .query(`UPDATE categorias 
                SET ? 
                WHERE id = ?`, [category, category.id], (error, resultado, fields) => {

                next(error)
            })
}

function deleteCategory(id, next) {
    CategoryModel
        .query(`DELETE FROM categorias 
                WHERE id = ?`, id, (error, resultado, fields) => {

                next(error)
            })
}

module.exports = {
    getCategoryById,
    getIdCategoryByName,
    getCategories,
    getNamesOfCategories,
    createCategory,
    updateCategory,
    deleteCategory
}